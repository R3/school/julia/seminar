{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "###### <br>\n",
    "<img src=\"graphics/julia.png\" width=\"20%\">\n",
    "\n",
    "# Julia & its applications \n",
    "\n",
    "<br>\n",
    "\n",
    "### Dr. Laurent Heirendt\n",
    "\n",
    "##### Luxembourg Centre for Systems Biomedicine\n",
    "\n",
    "##### April 28th, 2022 - Seminar MHH\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# What is Julia?\n",
    "\n",
    "\"Julia is a high-level, high-performance dynamic programming language […]\"\n",
    "\n",
    "**Features**\n",
    "- Multiple dispatch\n",
    "- Dynamic type system\n",
    "- Good performance \n",
    "- Call C functions directly\n",
    "- Powerful shell-like capabilities\n",
    "- Designed for parallelism \n",
    "- Free and open-source\n",
    "\n",
    "Visit http://julialang.org to get started!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Why Julia?\n",
    "\n",
    "Julia is a compromise between **convenience** and **performance**.\n",
    "\n",
    "\n",
    "<center><img src=\"graphics/whyjulia.png\" width=\"75%\"></center>\n",
    "\n",
    "\n",
    "\n",
    "*Source: Optimization Notice – Intel Corporation, STAC Summit, November 2015.*\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Benchmarks\n",
    "\n",
    "<br>\n",
    "<center><img src=\"graphics/benchmarks.png\" width=\"60%\"></center>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Solution to a linear system of equations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Solve the system of linear equations:\n",
    "\n",
    "$$\\mathbf{A}\\mathbf{x} = \\mathbf{b}$$\n",
    "\n",
    "with $\\mathbf{A}$ being defined as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "A = [1 2; 3 4]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Define a RHS vector $b$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "b = [1, 4]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Solve the system of equations using the built-in left division operator `\\`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "x = A\\b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Test if the solution obtained is correct:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "using Test\n",
    "@test A*x == b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Just-in-time (JIT) compilation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$\\newcommand{\\norm}[1]{\\left\\lVert#1\\right\\rVert}$$\n",
    "Declare the function $\\phi$:\n",
    "\n",
    "$$\\phi(\\alpha, \\textbf{u}) = \\alpha^{\\alpha-1} \\norm{\\textbf{u}}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "phi(alpha, u) = alpha.^(alpha-1) * sqrt(sum(u).^2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Benchmark 1.1**: Run `phi()` **without** typed function declaration and **before** first just-in-time (JIT) compilation run\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "benchmark11 = @elapsed phi(7, collect(1:1e7))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$\\rightarrow$ This is very powerful for quick prototyping of code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "**Benchmark 1.2**: Run `phi()` **without** typed function declaration but **after** first JIT compilation run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "benchmark12 = @elapsed phi(7, collect(1:1e7))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Speedup factor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "speedup = benchmark11 / benchmark12"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Multiple dispatch"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$$\\newcommand{\\norm}[1]{\\left\\lVert#1\\right\\rVert}$$\n",
    "Re-declare the function $\\phi$:\n",
    "\n",
    "$$\\phi(\\alpha, \\textbf{u}, \\beta) = \\alpha^{\\alpha-1} \\norm{\\textbf{u}} * \\beta$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "phi(alpha, u, beta) = alpha.^(alpha-1) * sqrt(sum(u).^2) * beta"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "The methods of $\\phi$ can be queried with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "methods(phi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Changing precision on-the-fly\n",
    "\n",
    "Sometimes, it is necessary to change the precision on-the-fly (faster calculations, higher precision for numerically challenging problems, ...)\n",
    "\n",
    "Julia allows to do that quite easily using the `setprecision` function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<br>\n",
    "<center><img src=\"graphics/precision.png\" height=\"60%\"></center>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "using Printf\n",
    "# single precision\n",
    "setprecision(22);\n",
    "a = BigFloat(1.0/3.0);\n",
    "@printf(\"The precision is set to SINGLE: a = %.60f\\n\", a);\n",
    "\n",
    "# double precision\n",
    "setprecision(52);\n",
    "a = BigFloat(1.0/3.0);\n",
    "@printf(\"The precision is set to DOUBLE: a = %.60f\\n\", a);\n",
    "\n",
    "# quad precision\n",
    "setprecision(112)\n",
    "a = BigFloat(1.0/3.0);\n",
    "@printf(\"The precision is set to   QUAD: a = %.60f\\n\", a);\n",
    "\n",
    "# single precision\n",
    "setprecision(22);\n",
    "a = BigFloat(1.0/3.0);\n",
    "@printf(\"The precision is set to SINGLE: a = %.60f\\n\", a);"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Julia's parallelization capabilities"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "### Add workers\n",
    "\n",
    "Let's add a few local processors to the pool:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "using Distributed\n",
    "@time addprocs(4)\n",
    "workers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Let's define a function to calculate the *p*-norm of a vector. \n",
    "\n",
    "In serial code, the function reads:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "function calculateNorm(vect, p)\n",
    "    # ...\n",
    "    # some input checking here, assuming p > 1\n",
    "    \n",
    "    sum = 0.0\n",
    "    for i = 1:length(vect)\n",
    "        sum += vect[i]^p\n",
    "    end\n",
    "    return sum^(1/p)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Let's define a vector `v` and an exponent `p` as an example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "v = collect(1:1e7)\n",
    "p = 4;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Benchmark the calculation of the norm (serial execution):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "using BenchmarkTools\n",
    "benchmarkSerial = @belapsed begin\n",
    "    global resultSerial \n",
    "    resultSerial = calculateNorm(v, p)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Parallelize the function (quick-and-dirty way)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Now, let us parallelize this code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using LinearAlgebra\n",
    "function calculateNorm(vect, p)\n",
    "    # ...\n",
    "    # some input checking here, assuming p > 1\n",
    "    \n",
    "    sum = 0\n",
    "    sum = @distributed (+) for i = 1:length(vect)\n",
    "        vect[i]^p\n",
    "    end\n",
    "    return sum^(1/p)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "benchmarkParallel = @belapsed begin \n",
    "    global resultParallel\n",
    "    resultParallel = calculateNorm(v, p) \n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "The default function norm may also be used in this case (just for reference):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "benchmarkStandard = @belapsed begin\n",
    "    global resultStandard\n",
    "    resultStandard = norm(v, p)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Confirm the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "using Test\n",
    "@test isapprox(resultStandard, resultSerial)\n",
    "@test isapprox(resultStandard, resultParallel)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Let's try to type the arguments of the function to improve its performance:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "function calculateNorm(vect::Array{Float64,1}, p::Int64)\n",
    "    # ...\n",
    "    # some input checking here, assuming p > 1\n",
    "    \n",
    "    sum::Float64 = 0.0\n",
    "    sum = @distributed (+) for i = 1:length(vect)\n",
    "        vect[i]^p\n",
    "    end\n",
    "    return sum^(1.0/p)\n",
    "end"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "benchmarkParallelTyped = @belapsed begin\n",
    "    global resultParallelTyped \n",
    "    resultParallelTyped = calculateNorm(v, p)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Confirm the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@test isapprox(resultSerial, resultParallel)\n",
    "@test isapprox(resultSerial, resultParallelTyped)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## A general parallelization method\n",
    "\n",
    "We may increase the performance further using the macro `@spawnat`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "@everywhere function calculateNorm(vect::Array{Float64,1}, p::Int64, lowerLimit::Int64, upperLimit::Int64)\n",
    "    # ...\n",
    "    # some input checking here, assuming p > 1\n",
    "    \n",
    "    sum::Float64 = 0.0\n",
    "    for i = lowerLimit:upperLimit\n",
    "        sum += vect[i]^p\n",
    "        end\n",
    "    return sum\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Enumerate the workers in the parallel pool:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pool = enumerate(workers())\n",
    "nWorkers = length(pool)\n",
    "\n",
    "# initialize Futures\n",
    "R = Array{Future}(UndefInitializer(), nWorkers, 1)\n",
    "\n",
    "# initialize the result\n",
    "result = 0.0;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "When generalizing the code, we obtain:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "benchmarkSpawn = @belapsed begin\n",
    "    global result\n",
    "    @sync for (proc, pid) in pool\n",
    "        # determine the chunk of the vector on worker p\n",
    "        chunkSize = convert(Int64, floor(length(v)/nWorkers))\n",
    "\n",
    "        # spawn the function to calculate the norm on each processor\n",
    "        R[proc] = @spawnat proc calculateNorm(v, p, 1 + (proc-1)*chunkSize, proc * chunkSize)\n",
    "    end\n",
    "\n",
    "    @sync for (proc, pid) in pool\n",
    "        global result\n",
    "        # fetch and add the result\n",
    "        result += fetch(R[proc])\n",
    "    end\n",
    "\n",
    "    # determine the result\n",
    "    result = result^(1/p)\n",
    "end"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "Verify the results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "@test isapprox(resultStandard, result)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Performance evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "We may now determine the speedup factors compared to the implementation using the `@spawnat` macro:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "#using Printf\n",
    "speedupParallel = (benchmarkParallel / benchmarkSpawn - 1) * 100\n",
    "speedupParallelTyped = (benchmarkParallelTyped / benchmarkSpawn - 1) * 100\n",
    "speedupSerial = (benchmarkSerial / benchmarkSpawn - 1) * 100\n",
    "\n",
    "println(\"$(@sprintf(\"%4.1f\", speedupParallel))% faster than the quick-and-dirty parallel implementation.\")\n",
    "println(\"$(@sprintf(\"%4.1f\", speedupParallelTyped))% faster than the typed quick-and-dirty parallel implementation.\")\n",
    "println(\"$(@sprintf(\"%4.1f\", speedupSerial))% faster than the typed serial implementation.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "$\\rightarrow$ **Decent** speedup for a generalized parallelization method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Stopping the parallel pool"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rmprocs(workers())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "workers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Applications"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "We use Julia on a daily basis, but primarily for large-scale high-performance applications:\n",
    "    \n",
    "<img src=\"graphics/gigasom-logo.png\" height=\"60%\"> GigaSOM.jl: https://git.io/GigaSOM.jl\n",
    "\n",
    "<img src=\"graphics/cobrexa-logo.png\" height=\"60%\"> COBREXA.jl: https://bit.ly/cobrexa"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## GigaSOM.jl\n",
    "\n",
    "Toolkit that enables the analysis and clustering of huge-scale flow cytometry data:\n",
    "\n",
    "- Horizontal scalability to literal giga-scale datasets (10^9 cells!)\n",
    "- HPC-ready, support for e.g. Slurm\n",
    "- Standard support for distributed loading, scaling and transforming the FCS3 files\n",
    "- Batch-SOM based GigaSOM algorithm for clustering\n",
    "- EmbedSOM for visualizations\n",
    "\n",
    "<br>\n",
    "\n",
    "*GigaSOM.jl: High-performance clustering and visualization of huge cytometry datasets \n",
    "Miroslav Kratochvíl, Oliver Hunewald, Laurent Heirendt, Vasco Verissimo, Jiří Vondrášek, Venkata P Satagopam, Reinhard Schneider, Christophe Trefois, Markus Ollert Author Notes\n",
    "GigaScience, Volume 9, Issue 11, November 2020, giaa127, https://doi.org/10.1093/gigascience/giaa127*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<center><img src=\"graphics/gigasom-dataset.png\" width=\"40%\"></center>\n",
    "\n",
    "Raw IMPC Spleen T-cell dataset, processed by GigaSOM.jl and embedded by the Julia implementation of EmbedSOM. The figure shows an aggregate of 1,167,129,317 individual cells. Expression of 3 main markers is displayed in combination as mixed colors: CD8 in red, CD4 in green, and CD161 in blue."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## COBREXA.jl\n",
    "\n",
    "Toolkit for working with large constraint-based metabolic models, and running very large numbers of analysis tasks on these models in parallel:\n",
    "\n",
    "- methods of Constraint-based Reconstruction and Analysis (COBRA) scale to problem sizes \n",
    "- optimized for huge computer clusters and HPC environments\n",
    "- ready to be applied to pre-exascale-sized models."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<center><img src=\"graphics/cobrexa-scalability.png\" width=\"60%\"></center>\n",
    "\n",
    "*Miroslav Kratochvíl, Laurent Heirendt, St Elmo Wilken, Taneli Pusa, Sylvain Arreckx, Alberto Noronha, Marvin van Aalst, Venkata P Satagopam, Oliver Ebenhöh, Reinhard Schneider, Christophe Trefois, Wei Gu, COBREXA.jl: constraint-based reconstruction and exascale analysis, Bioinformatics 38.4, 15 Feb 2022, Pages 1171–1172, https://doi.org/10.1093/bioinformatics/btab782.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Summary\n",
    "\n",
    "<center>\n",
    "<img src=\"graphics/julia.png\" width=\"10%\">\n",
    "</center>\n",
    "\n",
    "- Julia's strengths \n",
    "- Solution to a linear system of equations\n",
    "- JIT - Just-in-time compilation\n",
    "- Multiple dispatch\n",
    "- Changing precision on-the-fly\n",
    "- Julia's parallel capabilities\n",
    "- Applications"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Questions?\n",
    "\n",
    "<center>\n",
    "<img src=\"graphics/julia.png\" width=\"10%\">\n",
    "</center>\n",
    "\n",
    "lcsb-r3@uni.lu / miroslav.kratochvil@uni.lu / laurent.heirendt@uni.lu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "print(\"Thank you!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Interactive notebook available from:\n",
    "\n",
    "https://gitlab.lcsb.uni.lu/R3/school/julia/seminar"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Julia 1.7.1",
   "language": "julia",
   "name": "julia-1.7"
  },
  "language_info": {
   "file_extension": ".jl",
   "mimetype": "application/julia",
   "name": "julia",
   "version": "1.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
