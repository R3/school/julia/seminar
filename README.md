# Julia - the scientific programming language of the future

To get started on Julia:

```
] add IJulia
```

Then:

```
julia> using IJulia
```

To then run the notebook:

```
julia> notebook(pwd())
```

Then, click on the bar plot icon in the toolbar or hit `Alt-R`.

> Copyright 2021 Luxembourg Centre for Systems Biomedicine
>
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
>
>  http://www.apache.org/licenses/LICENSE-2.0
>
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.
